<nav class="navigation">
  <div class="row align-items-center">
    <div class="col-2">
      <h2 class="logo m-0">rad86</h2>
    </div>
    <div class="col-4">
      <input type="text" class="form-control" placeholder="Поиск">
    </div>
    <div class="col-6 text-right">
      <a href="#" class="mr-4">Избранное</a>
      <a href="#" class="mr-4">Вход и регистрация</a>
      <a href="#" class="btn btn-primary">Подать объявление</a>
    </div>
  </div>
</nav>
