<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" type="image/png" href="/img/favicon.png" sizes="16x16">
	<title>{{ config('app.name', 'Laravel') }}</title>

  <script>var csrfToken = "{{ csrf_token() }}";</script>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	{{-- <link href="{{ asset('css/bootstrap-datepicker3.min.css') }}?v{{ config('app.version_fixed') }}" rel="stylesheet"> --}}
	{{-- <link href="{{ asset('css/select2.min.css') }}?v={{ config('app.version_fixed') }}" rel="stylesheet"> --}}
	{{-- <link href="{{ asset('css/select2-bootstrap.min.css') }}?v{{ config('app.version_fixed') }}" rel="stylesheet"> --}}
	{{-- <link href="{{ asset('css/toastr.min.css') }}?v{{ config('app.version_fixed') }}" rel="stylesheet"> --}}

	<link href="{{ asset('css/base.css') }}?v{{ config('app.version') }}" rel="stylesheet">
  <link href="{{ asset('css/app.css') }}?v{{ config('app.version') }}" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

</head>
<body>
	<div id="app">
    <div class="container-xl">
      @include('layouts.header')

      <section class="main" id="main">
        @yield('content')
      </section>
  	  @include('layouts.footer')

      {{-- @include('layouts.scripts-footer') --}}
    </div>
  </div><!-- #app -->
</body>
</html>
