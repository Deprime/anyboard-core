@extends('layouts.app')
@section('content')

@php
  $ads_list = [
    [
      'type' => 1,
      'title' => 'СВАРОЧНЫЕ РАБОТЫ ЛЮБОЙ СЛОЖНОСТИ',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-svarochnye_raboty_lyuboy_slozhnost_195870.jpeg',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 'Цена договорная'
    ],
    [
      'type' => 1,
      'title' => 'Салон Штор "Гардинья"',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-otdelka_i_ustanovka_okon_balkonov_remont_kvartir_i_ofisov__tel___92-793_63113.jpeg',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 'Цена договорная'
    ],
    [
      'type' => 1,
      'title' => 'Ремонт Компьютеров И Ноутбуков В Радужном ХМАО 8-982-139-37-78',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-remont_kompyuterov_i_noutbukov_v_raduzhnom_hmao_8-982-139-37-78_195697.png',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 500
    ],
    [
      'type' => 1,
      'title' => 'Пластиковые Окна, Установка,',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-plastikovye_okna_remont_i_obshivka_balkonov_99607.jpeg',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 'Цена договорная'
    ],
    [
      'type' => 1,
      'title' => 'ЗооСалон "КотоПёс"',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-natyazhnye_potolki_top-okna_zhalyuzi_svetodiodnaya_produkciya__180418.jpeg',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 'Цена договорная'
    ],
    [
      'type' => 1,
      'title' => 'Эвакуатор, Теплый Запуск Автомобилей, Выездная Диагностика, Вск',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-evakuator_teplyy_zapusk_teh_pomosch_na_doroge__185334.jpeg',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 1000
    ],
    [
      'type' => 1,
      'title' => 'Пластиковые Окна Ремонт И Обшивка Балконов',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-remont_okon_otdelka_balkonov_v_raduzhnom_hmao_38078.jpeg',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 'Цена договорная'
    ],
    [ 'type' => 1,
      'title' => 'Эвакуатор, Теплый Запуск Автомобилей, Выездная Диагностика, Вск',
      'img' => 'http://www.86doska.ru/upload/normal/raduzhnyy_hmao-evakuator_teplyy_zapusk_teh_pomosch_na_doroge__185334.jpeg',
      'text' => 'Продам сноуборд',
      'date' => '14.10.2020',
      'price' => 1000
    ],
  ];
@endphp

<section>
  <div class="row">
    <div class="col-lg-10">
      <div class="row post-list">


        @foreach ($ads_list as $k => $ads)
          <div class="{{ ($ads['type'] == 1) ? 'col-lg-3' : 'col-lg-6' }}">
            <article class="post-item  type-{{ $ads['type'] }}">
              <a href="#" itemprop="url">
                <figure style="background-image: url('{{ $ads['img'] }}');">
                  {{-- <img src="{{ $ads['img'] }}" alt=""> --}}
                </figure>
              </a>
              <div class="desc">
                <a href="#" itemprop="url">
                  <h4 class="title" itemprop="name">
                    {{ $ads['title'] }}
                  </h4>
                </a>
                <div class="price">
                  @if (is_numeric($ads['price']))
                    {{ number_format($ads['price'], 0, ',', ' ') }} ₽
                  @else
                    {{ $ads['price'] }}
                  @endif
                </div>
                <div class="date">{{ $ads['date'] }}</div>
              </div>
            </article>
          </div>
        @endforeach

      </div>
    </div>
    <div class="col-lg-2"></div>
  </div>
</section>

{{-- @include('widgets.message-box') --}}
@endsection
